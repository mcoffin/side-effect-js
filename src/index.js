const { isPromise } = require('@mcoffin/promise-utils');

function sideEffect(f) {
    return (...args) => {
        const result = f(...args);
        if (isPromise(result)) {
            return result.then(() => args[0]);
        } else {
            return args[0];
        }
    };
}

module.exports = sideEffect;
