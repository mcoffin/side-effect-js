# side-effect-js

# Example

For slightly more complex examples, see [`test/test.js`](https://gitlab.com/mcoffin/side-effect-js/-/blob/master/test/test.js).

```javascript
const sideEffect = require('@mcoffin/side-effect');
const { expect } = require('chai');
Promise.resolve('foo')
    .then(sideEffect(console.log))
    .then((v) => expect(v).to.equal('foo'));
```
