const chai = require('chai');
const expect = chai.expect;

function timeoutPromise(duration) {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(), duration);
    });
}

describe('sideEffect', () => {
    const sideEffect = require('../src');

    it('should return the value passed in', () => {
        const f = sideEffect((v) => console.log('v:', v));
        expect(f(1)).to.equal(1);
        expect(f('foo')).to.equal('foo');
    });

    it('should perform side effects', () => {
        let value = null;
        const f = sideEffect((v) => {
            value = v;
        });
        expect(f('foo')).to.equal('foo');
        expect(value).to.equal('foo');
    });

    it('should return a promise when the function returns one', async () => {
        let value = null;
        const f = sideEffect(async (v) => {
            await timeoutPromise(100);
            console.log('v:', v);
            value = v;
        });
        await Promise.resolve('foo')
            .then((v) => {
                const ret = f(v);
                expect(ret).to.be.instanceOf(Promise);
            });
    });
});
